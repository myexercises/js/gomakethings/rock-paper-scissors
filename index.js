document.addEventListener('DOMContentLoaded', main);

function main() {
    class Game {
        constructor(rounds = []) {
            this.rounds = rounds;
        }
        count() {
            return this.rounds.length;
        }
        getLastRound() {
            return this.rounds[this.count()-1];
        }
        getRound(n = 0) {
            return this.count() === 0 ? 'start the game' : this.rounds[n-1];
        }
        getAllRounds() {
            return this.rounds;
        }
        addRound(round) {
            const isRoundEmpty = checkEmptyRound(round);
            if (isRoundEmpty) return this.rounds;
            return this.rounds.push(round);
        }
    }

    class Round {
        constructor() {
        }
        // private methods
        _randomChoice() {
            const choices = ['rock', 'paper', 'scissors'];
            const randomIndex = Math.floor(Math.random() * 3);
            return choices[randomIndex];
        }
        _checkForErrors() {
            const fieldsToCheck = ['playerA', 'playerB'];
            const errors = checkFields(this, fieldsToCheck);
            try {
                if (errors) {
                    throw `Something is wrong!`
                }
            } catch (error) {
                console.error(error);
                return errors;
            }
        }
        _setResult() {
            this.result = {
                'rock-rock': 'draw',
                'paper-paper': 'draw',
                'scissors-scissors': 'draw',
                'paper-rock': 'playerA',
                'rock-scissors': 'playerA',
                'scissors-paper': 'playerA',
                'paper-scissors': 'playerB',
                'rock-paper': 'playerB',
                'scissors-rock': 'playerB',
            }[`${this.playerA}-${this.playerB}`];
        }
        // public interface
        setChoiceA(choiceA = this._randomChoice()) {
            this.playerA = choiceA;
        }
        setChoiceB(choiceB = this._randomChoice()) {
            this.playerB = choiceB;
        }
        end() {
            const errors = this._checkForErrors();
            if (errors) return new Round();
            this._setResult();
            return this;
        }
    }

    class Stats {
        constructor(game) {
            this.game = game;
        }
        getResults(gameResult) {
            const result = checkResultPhraseCorrectness(gameResult);
            const roundResult = round => round.result.toLowerCase() === gameResult.toLowerCase();
            try {
                if (!result) {
                    throw `Can't filter ${gameResult}!`
                }
                const gameRounds = this.game.rounds;
                const filteredGame = gameRounds.filter(roundResult);
                return filteredGame;
            } catch (error) {
                console.error(error);
            }
        }
        getWinsCount(gameResult) {
            return this.getResults(gameResult).length;
        }
    }
    
    function displaySummary(round) {
        const isRoundEmpty = checkEmptyRound(round);
        const renderResult = () => round.result === 'draw' ? 'Draw' : `Winner is: ${round.result}`;
        if (isRoundEmpty) return `<p>Invalid round. Play again.</p>`;
        return `<p><strong>${round.playerA}</strong> (Player A) vs <strong>${round.playerB}</strong> (Player B)</p><h2>${renderResult()} </h2>`;
    }

    function checkFields(object, fields) {
        if (Object.keys(object).length === 0) {
            console.log('Empty object!');
            return true;
        }
        const errors = fields.reduce((errors, key) => {
            if (typeof object[key] === 'undefined') {
                console.log(`${key} doesn't exist!`);
                errors = true;
            }
            return errors;
        }, false);
        return errors;
    }

    function checkEmptyRound(round) { 
        return Object.keys(round).length === 0;
    }

    function checkResultPhraseCorrectness(gameResult) {
        const possibleResults = ['playerA', 'playerB', 'draw'];
        const result = possibleResults.map(r => r.toLowerCase()).includes(gameResult.toLowerCase());
        return result;
    }

    const game = new Game();
    const stats = new Stats(game);

    const buttonsParent = document.querySelector('p');
    const results = document.getElementById('results');
    const stat = document.querySelector('.stats');
    const statsPlayerA = document.querySelector('.stats__column--playerA');
    const statsPlayerB = document.querySelector('.stats__column--playerB');
    const statsDraw = document.querySelector('.stats__column--draw');

    stat.classList.add('stats--hidden');

    buttonsParent.addEventListener('click', handleButtonClick);

    function handleButtonClick(event) {
        const choiceA = event.target.getAttribute('data-choice');

        const round = new Round();
        round.setChoiceA(choiceA);
        round.setChoiceB();

        const playedRound = round.end();

        results.innerHTML = displaySummary(playedRound);

        game.addRound(playedRound);

        stat.classList.remove('stats--hidden');
        statsPlayerA.innerHTML = `<strong>Player A Wins:</strong> ${stats.getWinsCount('playerA')}`;
        statsPlayerB.innerHTML = `<strong>Player B Wins:</strong> ${stats.getWinsCount('playerB')}`;
        statsDraw.innerHTML = `<strong>Draws:</strong> ${stats.getWinsCount('draw')}`;
    }
}